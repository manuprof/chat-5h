<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request; // questo mi serve per l'oggetto Request
use Illuminate\Http\Response; // questo mi serve per l'oggetto Response

class MessagesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    // aggiorno il messaggio nel database
    public function update(Request $request,$id){
        $body=$request->input('body');
        
        $result=app('db')->update(
            "UPDATE messages SET body='$body' WHERE id=$id"
        );
        // verificare se il messaggio esisteva ed è stato aggiornato
        // controllo il numero di record aggiornati che dovrebbe essere = 1
        if($result > 0){
            return new Response(null,200);
        }
        else{
            // non è stato trova un messaggio
            return new Response(null,404);
        }
    }
   
}
